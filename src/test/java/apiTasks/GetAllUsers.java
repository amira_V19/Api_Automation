package apiTasks;

import org.testng.annotations.Test;

import static io.restassured.RestAssured.given;
public class GetAllUsers {

        @Test
        public void getAllUser() {

            given().
                    when().
                    get("https://reqres.in/api/users?per_page=12").
                    then().
                    log().
                    body().
                    assertThat().
                    statusCode(200);


    }
}
