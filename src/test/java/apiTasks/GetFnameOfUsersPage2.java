package apiTasks;

import org.testng.annotations.Test;

import static io.restassured.RestAssured.given;

public class GetFnameOfUsersPage2 {
@Test
        public void getFname(){
    var First = given().contentType("application/json")
            .when()
            .get("https://reqres.in/api/users?page=2")
            .then()
            .assertThat()
            .statusCode(200)
            .extract()
            .path("data.first_name");

                System.out.println(First.toString());
}}
