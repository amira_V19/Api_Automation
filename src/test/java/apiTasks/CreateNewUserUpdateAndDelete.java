package apiTasks;

import org.hamcrest.Matchers;
import org.json.simple.JSONObject;
import org.testng.annotations.Test;

import static io.restassured.RestAssured.given;

public class CreateNewUserUpdateAndDelete {
   public static   String userID;
    @Test(priority = 1)
    public void createNewUser() {
        JSONObject data = new JSONObject();

        data.put("name", "Arnold");
        data.put("Job", "Dev");

        var id = given().contentType("application/json")
                .body(data.toJSONString())
                .when()
                .post("https://reqres.in/api/users")
                .then()
                .log().all()
                .assertThat()
                .statusCode(201)
                .body("name", Matchers.equalTo("Arnold"))
                .extract()
                .path("id");
        userID = id.toString();

    }
    @Test(priority = 2)
    public void updateTheUserData() {

        JSONObject data = new JSONObject();

        data.put("name", "Sam");
        data.put("Job", "Dev");

        given().contentType("application/json")
                .body(data.toJSONString())
                .when()
                .put("https://reqres.in/api/users/"+userID)
                .then()
                .log().all()
                .assertThat()
                .statusCode(200)
                .body("name", Matchers.equalTo("Sam"))
        ;

    }
    @Test(priority = (3))
    public void deleteTheUser() {


        given().
                delete("https://reqres.in/api/users/"+userID).
                then()
                .log().all()
                .assertThat()
                .statusCode(204);

    }
}
